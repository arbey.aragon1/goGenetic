package main

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"

	"./algorithm"
	"./interfaceShared/dataPrice"
	"./paths"
	"./readData"
	"./utils"
	"./utilsGeneticAl/indiv"
	"./utilsGeneticAl/population"
)

const LEN_POP = 60

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	data := []dataPrice.DataPrice{}
	readData.ReadData(&data, paths.TRAIN_DATA)

	pop := population.RandPopulation(LEN_POP)

	dataFitness := []indiv.Indiv{}
	for i := 0; i < 1000; i++ {

		mutPop := population.MutationPopulation(&pop, 40)
		cPop := population.CrossPopulation(&pop, 20)

		pop = pop[:1]
		pop = append(pop, mutPop...)
		pop = append(pop, cPop...)
		population.Unique(&pop)

		fitnessPopCalc(&pop, &data)

		population.Sort(&pop)

		if len(pop) > LEN_POP {
			pop = pop[:LEN_POP]
		}

		if i%100 == 0 {
			fmt.Println("------", i)
			fmt.Println(pop[:3])
		}

		dataFitness = append(dataFitness, pop[0])
	}
	saveFitness(dataFitness)
	saveMeansOpt(pop[0].GetMeans(), pop[0].GetFitness())
}

func saveMeansOpt(data [2]int, fitness float32) {
	file, err := os.Create(paths.MEANS_TRAIN_OPT)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()
	fmt.Fprintf(file, "%d\t%d\t%f\n", data[0], data[1], fitness)
}

func saveFitness(data []indiv.Indiv) {
	file, err := os.Create(paths.FITNESS_LIST + strconv.Itoa(utils.RandInt(0, 100)) + ".csv")
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	for i, _ := range data {
		fmt.Fprintf(file, "%f\t%d\t%d\n", data[i].GetFitness(), data[i].GetMeans()[0], data[i].GetMeans()[1])
	}
}

func fitnessPopCalc(pop *[]indiv.Indiv, data *[]dataPrice.DataPrice) {
	ln := len(*pop)
	action := make(chan int)
	changeIter := make(chan bool)

	go referee(ln, action, changeIter)
	for i, _ := range *pop {
		go runIter(i, &(*pop)[i], data, action)
	}
	<-changeIter
}

func referee(ln int, action <-chan int, changeIter chan<- bool) {
	count := 0
	for count < ln {
		<-action
		count++
	}
	changeIter <- true
}

func runIter(val int, ind *indiv.Indiv, data *[]dataPrice.DataPrice, action chan<- int) {
	means := (*ind).GetMeans()
	test := new(algorithm.Algorithm)
	fit, _, _ := test.Run(means, data)

	if math.Abs(float64(means[0]-means[1])) < 3 {
		fit = fit * float32(0.0)
	}

	(*ind).SetFitness(fit)
	action <- val
}
