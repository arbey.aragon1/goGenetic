package algorithm

import (
	"../calculationMetrics/calcReturns"
	"../interfaceShared/dataPrice"
	"../interfaceShared/order"
	"../utils"
)

type Algorithm struct {
	id     int
	mLong  int
	mShort int
	status string
	m1_m2  int
}

func (this *Algorithm) Run(medias [2]int, dir *[]dataPrice.DataPrice) (float32, []order.Order, [][]dataPrice.DataPrice) {
	if medias[0] < medias[1] {
		this.mLong = medias[1]
		this.mShort = medias[0]
	} else {
		this.mLong = medias[0]
		this.mShort = medias[1]
	}

	this.status = utils.STATUS_INIT

	var arrayBuff [][]dataPrice.DataPrice
	this.buffer_1(&(*dir), &arrayBuff)

	var ordersData []order.Order
	this.algorithm(&arrayBuff, &ordersData)

	r1, _ := calcReturns.CalcReturns(&ordersData)
	return r1, ordersData, arrayBuff
}

func (this *Algorithm) buffer_1(array *[]dataPrice.DataPrice, arrayOut *[][]dataPrice.DataPrice) {
	for i, _ := range *array {
		if i >= this.mLong-1 {
			(*arrayOut) = append((*arrayOut), (*array)[i-this.mLong+1:i+1])
		}
	}
}

func (this *Algorithm) algorithm(array *[][]dataPrice.DataPrice, ordersData *[]order.Order) {
	// Buffer data
	changeDay := false
	var value []dataPrice.DataPrice
	valueOld := (*array)[0]
	i := 0
	for i, value = range *array {
		changeDay = (value[len(value)-1].GetDay() != valueOld[len(valueOld)-1].GetDay())
		if changeDay {
			this.changeDay(&value[len(value)-1], &(*ordersData))
		}

		mLongTem, mShorTem := this.meansCalc(&value)
		this.m1_m2 = utils.Sign(mLongTem - mShorTem)
		this.positionCalc(mLongTem, mShorTem, &value[len(value)-1], &(*ordersData))
		valueOld = value
		(*array)[i][len(value)-1].SetExtras(mLongTem, mShorTem, this.m1_m2)
	}
	if this.status != utils.STATUS_INIT {
		this.positionCloseFinal(&value[len(value)-1], &(*ordersData))
	}

}

func (this *Algorithm) meansCalc(item *[]dataPrice.DataPrice) (float32, float32) {
	mLongTem := utils.Mean((*item)[len(*item)-this.mLong:])
	mShorTem := utils.Mean((*item)[len(*item)-this.mShort:])
	return mLongTem, mShorTem
}

func (this *Algorithm) positionCalc(mLongTem float32, mShorTem float32, currentData *dataPrice.DataPrice, ordersData *[]order.Order) {
	lms := mLongTem > mShorTem
	sml := mShorTem > mLongTem

	if this.status == utils.STATUS_INIT {
		if sml {
			this.status = utils.LONG_POS
			this.orderBuy(&(*currentData), &(*ordersData))

		} else if lms {
			this.status = utils.SHORT_POS
			this.orderSell(&(*currentData), &(*ordersData))

		}
	} else if this.status == utils.LONG_POS {
		if lms {
			this.status = utils.STATUS_INIT
			this.orderSell(&(*currentData), &(*ordersData))
		}

	} else if this.status == utils.SHORT_POS {
		if sml {
			this.status = utils.STATUS_INIT
			this.orderBuy(&(*currentData), &(*ordersData))
		}
	}
}

func (this *Algorithm) positionCloseFinal(currentData *dataPrice.DataPrice, ordersData *[]order.Order) {
	if this.status == utils.LONG_POS {
		this.status = utils.STATUS_INIT
		this.orderSell(&(*currentData), &(*ordersData))

	} else if this.status == utils.SHORT_POS {
		this.status = utils.STATUS_INIT
		this.orderBuy(&(*currentData), &(*ordersData))

	}
}

func (this *Algorithm) changeDay(currentData *dataPrice.DataPrice, ordersData *[]order.Order) {
	ord := order.GetObj(currentData.GetTime(), currentData.GetPrice(), utils.CHANGE_DAY)
	(*ordersData) = append((*ordersData), ord)
}

func (this *Algorithm) orderBuy(currentData *dataPrice.DataPrice, ordersData *[]order.Order) {
	ord := order.GetObj(currentData.GetTime(), currentData.GetPrice(), utils.BUY_ORDER)
	(*ordersData) = append((*ordersData), ord)
}

func (this *Algorithm) orderSell(currentData *dataPrice.DataPrice, ordersData *[]order.Order) {
	ord := order.GetObj(currentData.GetTime(), currentData.GetPrice(), utils.SELL_ORDER)
	(*ordersData) = append((*ordersData), ord)
}
