package calcReturns

import (
	"../../interfaceShared/order"
	"../../print"
	"../../utils"
)

const INV = float32(2000.0)
const Q = float32(20.0)

func CalcReturns(ordersData *[]order.Order) (float32, float32) {
	var status string = utils.STATUS_INIT
	oldPrice := float32(0.0)
	profit := float32(0.0)
	count := float32(0.0)
	retOut := float32(0.0)

	investment := INV

	for i, v := range *ordersData {
		profit = float32(0.0)
		retOut = float32(0.0)
		updateStatus(&status, &v, &oldPrice, &profit, &retOut, &count)
		//print.PRINT(profit)
		investment = investment + profit
		(*ordersData)[i].SetInvAcVal(investment)
		(*ordersData)[i].SetRetVal(profit)

	}

	list := []float32{1, 2, 1, 2}
	print.PRINT(mean(list))

	retOfInversment := (investment - INV) / INV * float32(100.0)
	//print.PRINT("retorno promedio por operacion", retPromByOpt)
	//print.PRINT("retorno total", , "%")
	return retOfInversment, float32(0.0)
}

func mean(data *[]float32) float32 {
	print.PRINT("----------------")
	lenData := len(*data)
	print.PRINT(lenData)
	for _, v := range *data {
		print.PRINT(v)
	}
	return float32(0.0)
}

func updateStatus(status *string, orderData *order.Order, oldPrice *float32, profit *float32, retOut *float32, count *float32) {
	//print.PRINT(*orderData)
	if orderData.GetOrderType() == utils.BUY_ORDER {
		if (*status) == utils.STATUS_INIT {
			(*status) = utils.LONG_POS
			(*oldPrice) = orderData.GetPrice()

		} else if (*status) == utils.LONG_POS {
			print.PRINT("ERROR 1")

		} else if (*status) == utils.SHORT_POS {
			(*status) = utils.STATUS_INIT
			(*profit) = Q * ((*oldPrice) - orderData.GetPrice())
			(*retOut) = ((*oldPrice) - orderData.GetPrice()) / (*oldPrice)
			(*count) = (*count) + float32(1.0)
		}
	} else if orderData.GetOrderType() == utils.SELL_ORDER {
		if (*status) == utils.STATUS_INIT {
			(*status) = utils.SHORT_POS
			(*oldPrice) = orderData.GetPrice()

		} else if (*status) == utils.LONG_POS {
			(*status) = utils.STATUS_INIT
			(*profit) = Q * (orderData.GetPrice() - (*oldPrice))
			(*retOut) = (orderData.GetPrice() - (*oldPrice)) / (*oldPrice)
			(*count) = (*count) + float32(1.0)
		} else if (*status) == utils.SHORT_POS {
			print.PRINT("ERROR 2")

		}
	}
}
