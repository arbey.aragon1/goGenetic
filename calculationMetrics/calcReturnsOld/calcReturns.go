package calcReturns

import (
	"../../interfaceShared/order"
	"../../print"
	"../../utils"
)

func CalcReturns(ordersData *[]order.Order) (float32, float32) {
	var status string = utils.STATUS_INIT
	oldPrice := float32(0.0)
	retOut := float32(0.0)
	retTotal := float32(0.0)
	count := float32(0.0)

	investment := float32(1.0)

	for i, v := range *ordersData {
		retOut = float32(0.0)
		updateStatus(&status, &v, &oldPrice, &retOut, &count)
		//print.PRINT(retOut)
		investment = investment * (float32(1.0) + retOut)
		(*ordersData)[i].SetInvAcVal(investment)
		(*ordersData)[i].SetRetVal(retOut)
		retTotal = retTotal + retOut
	}
	retPromByOpt := retTotal / count
	retOfInversment := (investment - float32(1.0)) / float32(1.0) * float32(100.0)
	//print.PRINT("retorno promedio por operacion", retPromByOpt)
	//print.PRINT("retorno total", , "%")
	return retOfInversment, retPromByOpt
}

func updateStatus(status *string, orderData *order.Order, oldPrice *float32, retOut *float32, count *float32) {
	//print.PRINT(*orderData)
	if orderData.GetOrderType() == utils.BUY_ORDER {
		if (*status) == utils.STATUS_INIT {
			(*status) = utils.LONG_POS
			(*oldPrice) = orderData.GetPrice()

		} else if (*status) == utils.LONG_POS {
			print.PRINT("ERROR 1")

		} else if (*status) == utils.SHORT_POS {
			(*status) = utils.STATUS_INIT
			(*retOut) = ((*oldPrice) - orderData.GetPrice()) / (*oldPrice)
			(*count) = (*count) + float32(1.0)
		}
	} else if orderData.GetOrderType() == utils.SELL_ORDER {
		if (*status) == utils.STATUS_INIT {
			(*status) = utils.SHORT_POS
			(*oldPrice) = orderData.GetPrice()

		} else if (*status) == utils.LONG_POS {
			(*status) = utils.STATUS_INIT
			(*retOut) = (orderData.GetPrice() - (*oldPrice)) / (*oldPrice)
			(*count) = (*count) + float32(1.0)
		} else if (*status) == utils.SHORT_POS {
			print.PRINT("ERROR 2")

		}
	}
}
