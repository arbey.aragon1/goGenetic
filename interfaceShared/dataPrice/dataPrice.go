package dataPrice

import (
	"strconv"
)

type DataPrice struct {
	price  float32
	extras bool
	m1     float32
	m2     float32
	fos    int
	time   string
}

func (this *DataPrice) SetExtras(m1 float32, m2 float32, fos int) bool {
	this.extras = true
	this.m1 = m1
	this.m2 = m2
	this.fos = fos
	return true
}

func GetObj(time string, price float32) DataPrice {
	return DataPrice{time: time, price: price, extras: false, m1: 0, m2: 0, fos: 0}
}

func (this *DataPrice) GetDay() int {
	val, _ := strconv.Atoi(this.time[8:10])
	return val
}

func (this *DataPrice) GetPrice() float32 {
	return this.price
}

func (this *DataPrice) GetTime() string {
	return this.time
}

func (this *DataPrice) GetM1() float32 {
	return this.m1
}

func (this *DataPrice) GetM2() float32 {
	return this.m2
}

func (this *DataPrice) GetFos() int {
	return this.fos
}
