package order

type Order struct {
	time      string
	price     float32
	orderType string
	invAcVal  float32
	retVal    float32
}

func (this *Order) SetRetVal(retVal float32) {
	this.retVal = retVal
}

func (this *Order) GetRetVal() float32 {
	return this.retVal
}

func (this *Order) SetInvAcVal(invAcVal float32) {
	this.invAcVal = invAcVal
}

func (this *Order) GetInvAcVal() float32 {
	return this.invAcVal
}

func (this *Order) GetOrderType() string {
	return this.orderType
}

func (this *Order) GetPrice() float32 {
	return this.price
}

func (this *Order) GetTime() string {
	return this.time
}

func GetObj(time string, price float32, orderType string) Order {
	return Order{time: time, price: price, orderType: orderType}
}
