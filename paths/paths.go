package paths

const PATH_NOTEBOOK = "/home/arbey/Documentos/geneticmeanspaper/project/"
const INDEX_EXP = ""

const TRAIN_DATA = PATH_NOTEBOOK + "datosMuestraFiltrados/train" + INDEX_EXP + ".csv"
const TEST_DATA = PATH_NOTEBOOK + "datosMuestraFiltrados/test" + INDEX_EXP + ".csv"

const FITNESS_LIST = PATH_NOTEBOOK + "fitnessOut/fitness"

const MEANS_TEST_OPT = PATH_NOTEBOOK + "datosSalidaSimulacion/meansTestOpt" + INDEX_EXP + ".csv"
const MEANS_TRAIN_OPT = PATH_NOTEBOOK + "datosSalidaSimulacion/meansTrainOpt" + INDEX_EXP + ".csv"
const ORDERS_DATA = PATH_NOTEBOOK + "datosSalidaSimulacion/orders" + INDEX_EXP + ".csv"
const DATA_OUT = PATH_NOTEBOOK + "datosSalidaSimulacion/dataOut" + INDEX_EXP + ".csv"
const DATA_OUT_MEANS = PATH_NOTEBOOK + "datosSalidaSimulacion/dataOutMeans" + INDEX_EXP + ".csv"
