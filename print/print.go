package print

import "fmt"

func PRINT(args ...interface{}) {
	for _, v := range args {
		fmt.Print(" ")
		fmt.Print(v)
	}
	fmt.Print("\n") /**/
}
