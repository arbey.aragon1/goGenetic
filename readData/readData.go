package readData

import (
	"bufio"
	"os"
	"strconv"
	"strings"

	"../interfaceShared/dataPrice"
)

func ReadData(slice *[]dataPrice.DataPrice, filePath string) {
	//print.PRINT("-- Open file --")
	archivo, error := os.Open(filePath)
	defer func() {
		archivo.Close()
		//print.PRINT("-- Close file --")
	}()

	if error != nil {
		panic("Error al leer archivo!")
	}

	scanner := bufio.NewScanner(archivo)

	count := 0
	for scanner.Scan() {
		line := scanner.Text()
		value, err := strconv.ParseFloat(strings.Split(line, "\t")[1], 32)
		if err != nil {
			panic("Error al Convertir string!")
		}
		num := dataPrice.GetObj(strings.Split(line, "\t")[0], float32(value))
		(*slice) = append((*slice), num)

		count++
	}
}
