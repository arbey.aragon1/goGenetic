package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"./algorithm"
	"./interfaceShared/dataPrice"
	"./interfaceShared/order"
	"./paths"
	"./readData"
)

func main() {
	fmt.Println("Inicio")
	readFile()
	fmt.Println("Final")
}

func readFile() {

	means := readMeans()

	data := []dataPrice.DataPrice{}
	readData.ReadData(&data, paths.TEST_DATA)
	test := new(algorithm.Algorithm)
	fit, orders, dataMeans := test.Run(means, &data)
	fmt.Println(fit)

	saveMeansOpt(means, fit)
	saveData(data)
	saveDataMeans(dataMeans)
	saveOrders(orders)
}

func saveMeansOpt(data [2]int, fitness float32) {
	file, err := os.Create(paths.MEANS_TEST_OPT)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()
	fmt.Fprintf(file, "%d\t%d\t%f\n", data[0], data[1], fitness)
}

func readMeans() [2]int {
	archivo, error := os.Open(paths.MEANS_TRAIN_OPT)
	defer func() {
		archivo.Close()
	}()

	if error != nil {
		panic("Error!")
	}

	outMeans := [2]int{0, 0}
	scanner := bufio.NewScanner(archivo)

	for scanner.Scan() {
		line := scanner.Text()
		m1, err1 := strconv.ParseInt(strings.Split(line, "\t")[0], 0, 64)
		if err1 != nil {
			panic("Error al Convertir string m1!")
		}
		m2, err2 := strconv.ParseInt(strings.Split(line, "\t")[1], 0, 64)
		if err2 != nil {
			panic("Error al Convertir string m2!")
		}
		fmt.Println(m1, m2)
		outMeans[0] = int(m1)
		outMeans[1] = int(m2)
	}
	return outMeans
}

func saveDataMeans(data [][]dataPrice.DataPrice) {
	file, err := os.Create(paths.DATA_OUT_MEANS)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	for i, _ := range data {
		test := data[i][len(data[i])-1]
		fmt.Fprintf(file, "%s\t%f\t%f\t%d\n", test.GetTime(), test.GetM1(), test.GetM2(), test.GetFos())
	}

}

func saveData(data []dataPrice.DataPrice) {
	file, err := os.Create(paths.DATA_OUT)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	for i, _ := range data {
		fmt.Fprintf(file, "%s\t%f\n", data[i].GetTime(), data[i].GetPrice())
	}

}

func saveOrders(data []order.Order) {
	file, err := os.Create(paths.ORDERS_DATA)
	if err != nil {
		log.Fatal("Cannot create file", err)
	}
	defer file.Close()

	for i, _ := range data {
		fmt.Fprintf(file, "%s\t%f\t%s\t%f\t%f\n", data[i].GetTime(), data[i].GetPrice(), data[i].GetOrderType(), data[i].GetRetVal(), data[i].GetInvAcVal())
	}

}
