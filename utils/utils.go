package utils

import (
	"math/rand"

	"../interfaceShared/dataPrice"
)

const BUY_ORDER = "BUY_ORDER"
const SELL_ORDER = "SELL_ORDER"
const CHANGE_DAY = "CHANGE_DAY"

const STATUS_INIT = "STATUS_INIT"
const LONG_POS = "LONG_POS"
const SHORT_POS = "SHORT_POS"

func Mean(v []dataPrice.DataPrice) float32 {
	sumVal := float32(0.0)
	for i, _ := range v {
		sumVal += v[i].GetPrice()
	}
	return sumVal / float32(len(v))
}

func Sign(v float32) int {
	if v > 0 {
		return 1
	} else if v < 0 {
		return -1
	} else {
		return 0
	}
}

func RandInt(min int, max int) int {
	return min + rand.Intn(max-min+1)
}

func RandPatterns(ln int) (int, int) {
	p1 := 0
	p2 := 0
	for p1 == p2 {
		p1 = RandInt(0, ln)
		p2 = RandInt(0, ln)
	}
	return p1, p2
}
