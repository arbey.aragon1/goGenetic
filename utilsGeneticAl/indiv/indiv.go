package indiv

import (
	"../../utils"
	"github.com/cnf/structhash"
)

const MAX_DELTA = 20
const MAX_LIM = 180
const MIN_LIM = 5

type Indiv struct {
	calculated bool
	means      [2]int
	fitness    float32
}

func GetIndiv(means [2]int) Indiv {
	out := Indiv{means: means, fitness: 0, calculated: false}
	return out
}

func firstMayor(ind *Indiv) {
	medias := (*ind).GetMeans()
	if medias[0] < medias[1] {
		var m0 = medias[0]
		var m1 = medias[1]
		medias[0] = m1
		medias[1] = m0
		(*ind).SetMeans(medias)
	}
}

func (this *Indiv) Hash() string {
	hash, err := structhash.Hash(this.means, 1)
	if err != nil {
		panic(err)
	}
	return hash
}

func (this *Indiv) SetFitness(fit float32) {
	this.fitness = fit
	this.calculated = true
}

func (this *Indiv) SetMeans(means [2]int) {
	this.means = means
}

func (this *Indiv) GetCalculated() bool {
	return this.calculated
}

func (this *Indiv) GetFitness() float32 {
	return this.fitness
}

func (this *Indiv) GetMeans() [2]int {
	return this.means
}

func GetRandomIndiv() Indiv {
	means := [2]int{utils.RandInt(MIN_LIM, MAX_LIM), utils.RandInt(MIN_LIM, MAX_LIM)}
	return GetIndiv(means)
}

func Mutation(ind *Indiv) Indiv {
	if utils.RandInt(0, 1) == 1 {
		return mutationM1(ind)
	} else {
		return mutationM2(ind)
	}
}

func mutationM1(ind *Indiv) Indiv {
	means := ind.GetMeans()
	means[utils.RandInt(0, 1)] = utils.RandInt(MIN_LIM, MAX_LIM)
	return GetIndiv(means)
}

func mutationM2(ind *Indiv) Indiv {
	means := ind.GetMeans()
	i := utils.RandInt(0, 1)
	delta := (utils.RandInt(1, 2)*2 - 3) * utils.RandInt(1, MAX_DELTA)
	if means[i]+delta > MAX_LIM || means[i]+delta < MIN_LIM {
		means[i] = means[i] - delta
	} else {
		means[i] = means[i] + delta
	}
	return GetIndiv(means)
}

func Cross(p1 *Indiv, p2 *Indiv) (Indiv, Indiv) {
	meansP1 := p1.GetMeans()
	meansP2 := p2.GetMeans()
	if utils.RandInt(0, 1) == 1 {
		return GetIndiv([2]int{meansP2[0], meansP1[1]}), GetIndiv([2]int{meansP1[0], meansP2[1]})
	} else {
		return GetIndiv([2]int{meansP1[0], meansP2[1]}), GetIndiv([2]int{meansP2[0], meansP1[1]})
	}
}
