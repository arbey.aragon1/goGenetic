package population

import (
	"sort"

	"../../utils"
	"../indiv"
)

func RandPopulation(size int) []indiv.Indiv {
	pop := []indiv.Indiv{}
	for i := 0; i < size; i++ {
		pop = append(pop, indiv.GetRandomIndiv())
	}
	return pop
}

func MutationPopulation(pop *[]indiv.Indiv, indiv_mutation int) []indiv.Indiv {
	ln := len(*pop)
	newPop := []indiv.Indiv{}
	index := 0
	for i := 0; i < indiv_mutation; i++ {
		index = utils.RandInt(0, ln-1)
		h1 := indiv.Mutation(&(*pop)[index])
		newPop = append(newPop, h1)
	}
	return newPop
}

func CrossPopulation(pop *[]indiv.Indiv, indiv_cross int) []indiv.Indiv {
	ln := len(*pop)
	newPop := []indiv.Indiv{}
	ip1, ip2 := 0, 0
	for i := 0; i < int(indiv_cross/2); i++ {
		ip1, ip2 = utils.RandPatterns(ln - 1)
		h1, h2 := indiv.Cross(&(*pop)[ip1], &(*pop)[ip2])
		newPop = append(newPop, h1)
		newPop = append(newPop, h2)
	}
	return newPop
}

func Sort(array *[]indiv.Indiv) {
	sort.Slice((*array), func(i, j int) bool {
		return (*array)[i].GetFitness() > (*array)[j].GetFitness()
	})
}

func Unique(array *[]indiv.Indiv) {
	keys := make(map[string]bool)
	list := []indiv.Indiv{}
	entry := ""
	for _, val := range *array {
		entry = val.Hash()
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, val)
		}
	}
	(*array) = list
}
